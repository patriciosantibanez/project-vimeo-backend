import { Module } from '@nestjs/common';
import { AppController } from './controller/app.controller';
import { SearchList } from 'src/context/user/application/searchList';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [SearchList],
})
export class AppModule {}
