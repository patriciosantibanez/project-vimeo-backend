export interface UserDTO {
  name: string;
  title: string;
  subtitle: string;
  video: string;
}
